<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Création des utilisateurs Admin
        $adminUsers = [
            [
                'name' => 'Admin',
                'email' => 'admin1997@gmail.com',
                'password' => bcrypt('Admin1997'),
                'role' => 'Admin',
                'email_verified_at' => null,
                'remember_token' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
           
        ];

        foreach ($adminUsers as $adminUserData) {
            User::create($adminUserData);
        }
    }
}
