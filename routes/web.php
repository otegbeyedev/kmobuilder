<?php

use App\Http\Controllers\KmoController\AdminController;
use App\Http\Controllers\KmoController\TemplateController;
use App\Http\Controllers\KmoController\TypeComponentController;
use App\Http\Controllers\KmoController\PageController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Home', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('home'); 


Route::get('/builder', function () {
    return Inertia::render('Builder');
});

/**Dashboard */
// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

/**Builder */
Route::get('/builder', function () {
    return Inertia::render('Builder');
});

// Page
Route::post('/builderPage/store', [PageController::class, "store"])->middleware(['auth', 'verified'])->name('builderPage.store');
Route::post('/builderPage/index', [PageController::class, "index"])->middleware(['auth', 'verified'])->name('builderPage.index');
Route::post('/getpagecomponents', [PageController::class, "getpagecomponents"])->middleware(['auth', 'verified'])->name('getpagecomponents');
Route::post('/addSousBlocDirect', [PageController::class, "addSousBlocDirect"])->middleware(['auth', 'verified'])->name('addSousBlocDirect');
Route::post('/getSousBloc', [PageController::class, "getSousBloc"])->middleware(['auth', 'verified'])->name('getSousBloc');

/**Admin */
Route::get('/admin', [AdminController::class, "index"])->middleware(['auth', 'verified'])->name('admin');
Route::post('/admin/searchTemplateName', [AdminController::class, "searchTemplateName"])->middleware(['auth', 'verified'])->name('admin.searchTemplateName');
Route::post('/admin/orderTemplateName', [AdminController::class, "orderTemplateName"])->middleware(['auth', 'verified'])->name('admin.orderTemplateName');
Route::post('/admin/orderTemplateNameDesc', [AdminController::class, "orderTemplateNameDesc"])->middleware(['auth', 'verified'])->name('admin.orderTemplateNameDesc');
Route::post('/admin/destroyTemplate', [AdminController::class, "deleteTemplate"])->middleware(['auth', 'verified'])->name('admin.destroyTemplate');

Route::get('/admin/templatesEmpty', [TemplateController::class, "templatesEmpty"])->middleware(['auth', 'verified'])->name('admin.templatesEmpty');
Route::post('/admin/searchTemplateEmpty', [AdminController::class, "searchTemplateEmpty"])->middleware(['auth', 'verified'])->name('admin.searchTemplateEmpty');

Route::get('/admin/user', [AdminController::class, "allsUsers"])->middleware(['auth', 'verified'])->name('admin.index');
Route::post('/admin/updateuser', [AdminController::class, "updateUser"])->middleware(['auth', 'verified'])->name("admin.updateuser");
Route::post('/admin/orderUser', [AdminController::class, "orderUser"])->middleware(['auth', 'verified'])->name('admin.orderUser');
Route::post('/admin/orderUserDesc', [AdminController::class, "orderUserDesc"])->middleware(['auth', 'verified'])->name('admin.orderUserDesc');
Route::post('/admin/searchUser', [AdminController::class, "searchUser"])->middleware(['auth', 'verified'])->name('admin.searchUser');
Route::post('/admin/destroy', [AdminController::class, "destroy"])->middleware(['auth', 'verified'])->name('admin.destroy');

Route::get('/superAdmin/create', [AdminController::class, "superIndex"])->middleware(['auth', 'verified']);
Route::post('/superAdmin/createAdmin', [AdminController::class, "createAdmin"])->middleware(['auth', 'verified'])->name("create.admin");


/**TypeComponents */
Route::post('/typecomponent/index', [TypeComponentController::class, "index"])->name('typecomponent.index');
Route::post('/typecomponent/store', [TypeComponentController::class, "store"])->name('typecomponent.store');


/** Template */
Route::get('/dashboard', [TemplateController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');
Route::post('/template/store', [TemplateController::class, "store"])->name('template.store');
Route::get('/template/show/{id}', [TemplateController::class, 'show'])->name('template.show');
Route::post('/template/destroy', [TemplateController::class, "destroy"])->name('template.destroy');



/**Profiles */
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
