<?php

namespace App\Models\Kmo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use HasFactory;

    protected $fillable = [
        'component_code',
        'component_type',
        'component_html',
        // 'component_id'
    ];


    public function componentType()
{
    return $this->belongsTo(ComponentType::class, 'id');
}

}
