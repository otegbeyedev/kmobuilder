<?php

namespace App\Models\Kmo;

use App\Models\Kmo\Template;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pagetemplate extends Model
{
    use HasFactory;
    protected $fillable = [
        'page_name',
        'template_id'
    ];

    /**
     * Une page appartient à un seul template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function components()
    {
        return $this->hasMany(Pagecomponent::class, 'ptemplate_id');
    }

    // public function pagecomponents()
    // {
    //     return $this->hasMany(Pagecomponent::class, 'ptemplate_id', 'id');
    // }

    public function pagecomponents()
{
    return $this->hasMany(Pagecomponent::class, 'ptemplate_id');
}

}
