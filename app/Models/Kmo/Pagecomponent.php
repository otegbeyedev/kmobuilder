<?php

namespace App\Models\Kmo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pagecomponent extends Model
{
    use HasFactory;

    protected $fillable = [
        'ptemplate_id',
        'component_id'
    ];

    /**
     * Un component appartient à une seul page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    /**
     * Un component appartient à une seul page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function component()
    {
        return $this->belongsTo(Component::class, 'component_id');
    }
}
