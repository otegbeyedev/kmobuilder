<?php

namespace App\Models\Kmo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LiaisonComponents extends Model
{
    use HasFactory;
    protected $fillable = [
        'child',
        'parent',
        'big_parent'
    ];
}
