<?php

namespace App\Models\Kmo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeComponent extends Model
{
    use HasFactory;

    protected $fillable = [
        'svg',
        'type',
        'balise',
        'html',
    ];
}
