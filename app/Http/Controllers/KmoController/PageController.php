<?php


namespace App\Http\Controllers\KmoController;

use App\Http\Controllers\Controller;
use App\Models\Kmo\Component;
use App\Models\Kmo\LiaisonComponents;
use App\Models\Kmo\Pagecomponent;
use App\Models\Kmo\Template;
use App\Models\kmo\Pagetemplate;
use App\Models\Kmo\TypeComponent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $templateId = $request->template_id;

            $pages = PageTemplate::whereHas('pageComponents', function ($query) use ($templateId) {
                $query->where('template_id', $templateId);
            })->get();

            return $pages;
        } catch (\Exception $th) {
            dd($th);
        }
    }

    public function getpagecomponents(Request $request)
    {
        $pageId = $request->page_id;

        $components = [];
        $header = Component::join('pagecomponents', 'pagecomponents.component_id', '=', 'components.id')
            ->where("component_type", "=", "header")
            ->where("ptemplate_id", "=", $pageId)
            ->get()->toArray();
        if (count($header) > 0) {
            array_push($components, $header[0]);
        }

        $main = Component::join('pagecomponents', 'pagecomponents.component_id', '=', 'components.id')
            ->where("component_type", "=", "main")
            ->where("ptemplate_id", "=", $pageId)
            ->get()->toArray();
        if (count($main) > 0) {
            array_push($components, $main[0]);
        }

        $footer = Component::join('pagecomponents', 'pagecomponents.component_id', '=', 'components.id')
            ->where("component_type", "=", "footer")
            ->where("ptemplate_id", "=", $pageId)
            ->get()->toArray();
        if (count($footer) > 0) {
            array_push($components, $footer[0]);
        }

        $html = implode("", $this->codeHtml($pageId));
        // dd($html);
        return $components;
    }

    // fonction qui génère le code html de la page
    public function codeHtml($page_id)
    {
        $blocsPrincipaux = ["header", "main", "footer"];
        $bigArray = [];
        for ($i = 0; $i < 1; $i++) {
            $result = Component::join('pagecomponents', 'pagecomponents.component_id', '=', 'components.id')
                ->where("component_type", "=", $blocsPrincipaux[$i])
                ->where("ptemplate_id", "=", $page_id)
                ->get()->toArray();
            if (count($result) > 0) {
                $id = $result[0]["id"];
                $results = Component::join('liaison_components', 'liaison_components.big_parent', '=', 'components.id')
                    ->where("liaison_components.big_parent", "=", $id)
                    ->get()->toArray();
                $tab = [];
                $element = [];
                foreach ($results as $key => $values) {
                    $arr = [];
                    if ($values["parent"] !== "" && $values["parent"] !== null) {
                        $parent = LiaisonComponents::where("parent", "=", $values["parent"])
                            ->get()->toArray();
                        $parent_directs = Component::where("id", "=", $values["parent"])
                            ->get()->toArray();
                        $typeparent = $parent_directs[0]['component_type'];
                        $idparent = $parent_directs[0]['component_code'];
                        $codeparent = $parent_directs[0]['component_html'];
                        foreach ($parent as $par => $child) {
                            $childs = Component::where("id", "=", $child["child"])
                                ->get()->toArray();
                            array_push($arr, $childs[0]["component_html"]);
                        }
                        $implode = implode("", $arr);
                        if (preg_match("/(?<debut><$typeparent\s*id=\'$idparent\'\s*class=\'.*\'.*>)(?<contenu>.*)(?<fin><\/$typeparent\>)/i", $codeparent, $match)) {
                            $codeparent = preg_replace("/<$typeparent\s*id=\'$idparent\'\s*class=\'.*\'\s*>/i", $match["debut"] . $match["contenu"] . $implode, $codeparent);
                            array_push($element, $match["debut"] . "<\/$typeparent\>");
                        }
                        array_push($tab, $codeparent);
                        $tab = array_unique($tab);
                        $element = array_unique($element);
                    }
                }

                for ($z = 0; $z < count($tab); $z++) {
                    for ($w = 1; $w < count($tab); $w++) {
                        if (array_key_exists($w, $tab)) {
                            $el = $element[$w];
                            if (preg_match("/$el/i", $tab[$z], $match)) {
                                $tab[$z] = preg_replace("/$el/i", $tab[$w], $tab[$z]);
                                unset($tab[$w]);
                            }
                        }
                    }
                }

                $headerHtml = $result[0]["component_html"];
                $headerCode = $result[0]["component_code"];
                $headerType = $result[0]["component_type"];
                $codeImplode = implode("", $tab);
                if (preg_match("/(?<debut><$headerType\s*id=\'$headerCode\'\s*class=\'.*\'.*>)(?<contenu>.*)(?<fin><\/$headerType\>)/i", $headerHtml, $match)) {
                    $headerHtml = preg_replace("/<$headerType\s*id=\'$headerCode\'\s*class=\'.*\'\s*>/i", $match["debut"] . $match["contenu"] . $codeImplode, $headerHtml);
                }
                array_push($bigArray, $headerHtml);
            } else {
                array_push($bigArray, $result[0]["component_html"]);
            }
        }
        return $bigArray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $allsPages = Pagetemplate::where("template_id", "=", $request->template_id)->get()->toArray();
            $count = 1;
            if (count($allsPages) > 0) {
                $count = count($allsPages) + 1;
            }
            $verify = Pagetemplate::where("page_name", "=", "Page $count")->where("template_id", "=", $request->template_id)->get()->toArray();
            if (count($verify) == 0) {
                // Créer une nouvelle page
                $page = Pagetemplate::create([
                    'page_name' => "Page $count",
                    'template_id' => "$request->template_id"
                ]);

                // Récupération de l'id de la page créée
                $verify = Pagetemplate::where("page_name", "=", "Page $count")->where("template_id", "=", $request->template_id)->get()->toArray();
                $idPage = $verify[0]["id"];

                // Génération des composants de bases et insertion dans les tables components et pagecomponents
                $header = "<header id='' class=''></header>";
                $main = "<main id='' class=''></main>";
                $footer = "<footer id='' class=''></footer>";
                $balises = array($header, $main, $footer);
                $types = array("header", "main", "footer");

                for ($i = 0; $i < count($balises); $i++) {
                    $code = uniqid('cop_', true);
                    $code = explode(".", $code)[0];
                    $existingComponent = Component::where("component_code", '=', $code)->get()->toArray();
                    if (count($existingComponent) == 0) {
                        $balises[$i] = preg_replace("/id=''/i", "id='$code'", $balises[$i]);

                        // insertion dans la table component
                        $insert = Component::create([
                            'component_code' => $code,
                            'component_type' => $types[$i],
                            'component_html' => $balises[$i]
                        ]);
                        $existing = Component::where("component_code", '=', $code)->get()->toArray();

                        // insertion dans la table pagecomponent
                        $insertPageComponent = Pagecomponent::create([
                            "ptemplate_id" => $idPage,
                            "component_id" => $existing[0]["id"]
                        ]);
                    }
                }
            }

            $templateId = $request->template_id;
            $pages = PageTemplate::whereHas('pageComponents', function ($query) use ($templateId) {
                $query->where('template_id', $templateId);
            })
                ->get();


            return $pages;
        } catch (\Exception $th) {
            dd($th);
        }
    }

    // fonction pour ajouter des sous-blocs directement liés au(x) grand(s) bloc(s) (header/main/footer) dans la table components
    public function addSousBlocDirect(Request $request)
    {
        try {
            $idPage = $request->page_id;
            $bigBloc = $request->bigBloc;
            $codeType = TypeComponent::where('balise', '=', $request->codeBalise)->get()->toArray();
            $codeHtml = $codeType[0]["html"];
            $code = uniqid('cop_', true);
            $code = explode(".", $code)[0];
            if (preg_match("/id=''/i", $codeHtml)) {
                $codeHtml = preg_replace("/id=''/i", "id='$code'", $codeHtml);
            } else {
                $codeHtml = preg_replace("/<$request->codeBalise/i", "<$request->codeBalise id='$code'", $codeHtml);
            }
            // insertion dans la table component
            $insert = Component::create([
                'component_code' => $code,
                'component_type' => $request->codeBalise,
                'component_html' => $codeHtml
            ]);
            $existing = Component::where("component_code", '=', $code)->get()->toArray();

            // insertion dans la table pagecomponent
            $insertPageComponent = Pagecomponent::create([
                "ptemplate_id" => $idPage,
                "component_id" => $existing[0]["id"]
            ]);

            // insertion dans la table liaison_component
            $insertLiaisonComponent = LiaisonComponents::create([
                "child" => $existing[0]["id"],
                "parent" => null,
                "big_parent" => $bigBloc
            ]);
        } catch (\Exception $th) {
            dd($th);
        }
    }

    // fonction pour ajouter des sous-blocs directement à des sous-blocs dans la table components
    public function addSousBloc(Request $request)
    {
        try {
            $idPage = $request->page_id;
            $parentDirect = $request->parentDirect;
            $bigBloc = $request->bigBloc;
            $codeType = TypeComponent::where('balise', '=', $request->codeBalise)->get()->toArray();
            $codeHtml = $codeType[0]["html"];
            $code = uniqid('cop_', true);
            $code = explode(".", $code)[0];
            if (preg_match("/id=''/i", $codeHtml)) {
                $codeHtml = preg_replace("/id=''/i", "id='$code'", $codeHtml);
            } else {
                $codeHtml = preg_replace("/<$request->codeBalise/i", "<$request->codeBalise id='$code'", $codeHtml);
            }
            // insertion dans la table component
            $insert = Component::create([
                'component_code' => $code,
                'component_type' => $request->codeBalise,
                'component_html' => $codeHtml
            ]);
            $existing = Component::where("component_code", '=', $code)->get()->toArray();

            // insertion dans la table pagecomponent
            $insertPageComponent = Pagecomponent::create([
                "ptemplate_id" => $idPage,
                "component_id" => $existing[0]["id"]
            ]);

            // insertion dans la table liaison_component
            $insertLiaisonComponent = LiaisonComponents::create([
                "child" => $existing[0]["id"],
                "parent" => $parentDirect,
                "big_parent" => $bigBloc
            ]);
        } catch (\Exception $th) {
            dd($th);
        }
    }

    public function getSousBloc(Request $request)
    {
        dd($request->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    }
}