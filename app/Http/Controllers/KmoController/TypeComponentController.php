<?php

namespace App\Http\Controllers\KmoController;

use App\Http\Controllers\Controller;
use App\Models\Kmo\TypeComponent;
use App\Models\Kmo\Component;
use Illuminate\Http\Request;

class TypeComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $element = Component::where("id", "=", $request->id)->get()->toArray();
        $typeComp = strtolower($element[0]["component_type"]);
        // dd($element, $typeComp);
        switch($typeComp) {
            case 'header':
                $all_typeComponent = TypeComponent::whereIn("type", ['image', 'Espace de navigation', 'Liste', 'Lien', 'Espace de remplissage', 'Grand Titre'])->get()->toArray();
            return json_encode($all_typeComponent);
        }
        
    }

    public function typeComponent(Request $request)
    {
        $element = "";
        switch($element) {
            Case 'header':
            return json_encode($element);
            Case 'main':
            return json_encode($element);
            Case 'footer':
            return json_encode($element);
            Case 'nav':
            return json_encode($element);
            Case 'div':
            return json_encode($element);
            Case 'ul':
            return json_encode($element);
            Case 'ol':
            return json_encode($element);
            Case 'li':
            return json_encode($element);
            Case 'a':
            return json_encode($element);
            Case 'p':
            return json_encode($element);
            Case 'section':
            return json_encode($element);
            Case 'article':
            return json_encode($element);
            Case 'figure':
            return json_encode($element);
            Case 'figcaption':
            return json_encode($element);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // dd($request);
            $result = TypeComponent::where("type", "=", $request->component_name)->get()->toArray();
            // dd($result);

            if(count($result) == 0)
            {
                TypeComponent::create([
                    'svg' => $request->component_svg,
                    'type' => $request->component_name,
                    'balise' => $request->component_balise,
                    'html' => $request->component_html,
                ]);
    
                return response(["result" => true]);
            } else {
                return response(["error" => "Ce composant existe déjà !!!"]);
            }

        } catch (\Exception $th) {
            dd($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kmo\TypeComponent  $typeComponent
     * @return \Illuminate\Http\Response
     */
    public function show(TypeComponent $typeComponent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kmo\TypeComponent  $typeComponent
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeComponent $typeComponent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kmo\TypeComponent  $typeComponent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeComponent $typeComponent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kmo\TypeComponent  $typeComponent
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeComponent $typeComponent)
    {
        //
    }
}