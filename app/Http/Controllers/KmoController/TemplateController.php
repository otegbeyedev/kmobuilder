<?php

namespace App\Http\Controllers\KmoController;

use App\Http\Controllers\Controller;
use App\Models\Kmo\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Inertia\Inertia;
use Illuminate\Support\Str;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale('fr');
        Carbon::setLocale('fr');
        // Récupérer le modèle à afficher en fonction de l'ID
        $user = Auth::user();
        $templates = Template::where('user_id', $user->id)->get();

        $templates->transform(function ($template) {
            $lastUpdated = Carbon::parse($template->updated_at);
            $diffInMinutes = $lastUpdated->diffInMinutes();
            $diffInHours = $lastUpdated->diffInHours();
            $diffInDays = $lastUpdated->diffInDays();
            $diffInMonths = $lastUpdated->diffInMonths();
            $diffInYears = $lastUpdated->diffInYears();

            if ($diffInMinutes < 60) {
                $formattedDiff = $lastUpdated->diffForHumans(['parts' => 1]);
            } elseif ($diffInHours < 24) {
                $formattedDiff = $lastUpdated->diffForHumans(['parts' => 2]);
            } elseif ($diffInDays < 30) {
                $formattedDiff = $lastUpdated->diffForHumans(['parts' => 3]);
            } elseif ($diffInMonths < 12) {
                $formattedDiff = $lastUpdated->diffForHumans(['parts' => 4]);
            } else {
                $formattedDiff = $lastUpdated->diffForHumans(['parts' => 5]);
            }

            return [
                'id' => $template->id,
                'uuid' => $template->uuid,
                'template_name' => strtoupper($template->template_name),
                'last_updated' => $formattedDiff,
            ];
        });

        return Inertia::render('Dashboard', [
            'templates' => $templates,
            'flash' => [
                'success' => session('success'),
                'error' => session('error'),
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $id = Auth::user()->id;

            // Vérifier si le projet existe déjà
            $existingTemplate = Template::where('user_id', $id)
                ->where('template_name', $request->templateName)
                ->first();

            if ($existingTemplate) {

                // Le projet existe déjà
                // Afficher un message d'erreur ou effectuer d'autres actions appropriées
                session()->flash('error', 'Un template existe déja avec ce nom');
                return back();
            } else {


                // Générer l'UUID aléatoire
                $uuid = str_replace(' ', '-', strtolower($request->templateName)) . '-' . Str::random(8);
                ;


                // Créer un nouveau projet
                $template = Template::create([
                    'uuid' => $uuid,
                    'template_name' => $request->templateName,
                    'template_price' => null,
                    'status' => 'Public',
                    'user_id' => $id, // Utilisateur connecté
                ]);


                session()->flash('success', 'Projet créé avec succès');
                return redirect()->route("template.show", $template->uuid);
            }
        } catch (\Exception $th) {
            dd($th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        // Récupérer le modèle à afficher en fonction de l'ID
        $template = Template::where('uuid', $uuid)->firstOrFail();

        return Inertia::render('Template/Show', [
            'template' => $template,
        ]);
    }


    public function renameTemplate($uuid, $newTemplateName)
    {
        try {
            $template = Template::findOrFail($uuid);
            $template->template_name = $newTemplateName;
            $template->save();

            return redirect()->route('dashboard', $template->id)->with('success', 'Nom du template modifié avec succès.');
        } catch (\Exception $th) {
            return back()->with('error', 'Template non trouvé.');
        }
    }

    // fonction qui affiche tous les templates qui n'ont pas de pages By Kola
    public function templatesEmpty()
    {
        // requête pour récupérer les templates qui n'ont pas de pages
        $templatesNotPages = Template::leftJoin('pagetemplates', 'templates.id', '=', 'pagetemplates.template_id')
            ->join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as templateId', 'template_name', 'templates.status', 'templates.created_at', 'users.name')
            ->whereNull('pagetemplates.template_id')
            ->orderBy("templates.created_at", "desc")->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->templateId),
                'template_name' => $template->template_name,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);

        $templatesNotPage = [];
        if (count($templatesNotPages) > 0) {
            for ($i = 0; $i < count($templatesNotPages); $i++) {
                $now = time();
                $dat = strtotime($templatesNotPages[$i]["created_at"]);
                $diff = abs($now - $dat);
                $second = $diff % 60;
                $diff = floor(($diff - $second) / 60);
                $minute = $diff % 60;
                $diff = floor(($diff - $minute) / 60);
                $hour = $diff % 24;
                $diff = floor(($diff - $hour) / 24);
                $day = (int) $diff;
                if ($day == 15) {
                    // Suppression d'un template après 15 jours s'il n'a pas de pages
                    $id = Crypt::decryptString($templatesNotPages[$i]["id"]);
                    Template::destroy($id);
                }
            }

            for ($i = 0; $i < count($templatesNotPages); $i++) {
                $now = time();
                $dat = strtotime($templatesNotPages[$i]["created_at"]);
                $diff = abs($now - $dat);
                $second = $diff % 60;
                $diff = floor(($diff - $second) / 60);
                $minute = $diff % 60;
                $diff = floor(($diff - $minute) / 60);
                $hour = $diff % 24;
                $diff = floor(($diff - $hour) / 24);
                $day = (int) $diff;
                if ($day < 15) {
                    $x = 15;
                    if ($x > 0) {
                        $x = $x - $day;
                    } else {
                        $x = 0;
                    }
                    $templatesNotPage[$i]["id"] = $templatesNotPages[$i]["id"];
                    $templatesNotPage[$i]["template_name"] = $templatesNotPages[$i]["template_name"];
                    $templatesNotPage[$i]["name"] = $templatesNotPages[$i]["name"];
                    $templatesNotPage[$i]["status"] = $templatesNotPages[$i]["status"];
                    $templatesNotPage[$i]["created_at"] = $templatesNotPages[$i]["created_at"];
                    $templatesNotPage[$i]["number_day"] = $x;
                }
            }
        }
        return Inertia::render('Admin/TemplateEmpty', [
            'templatesNotPage' => $templatesNotPage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Template $template)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kmo\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // Récupérer l'ID de l'élément à supprimer à partir de la requête
        $id = $request->input('templateId');


        // Supprimer le projet lui-même
        Template::destroy($id);

        // Retourner une réponse indiquant que l'élément a été supprimé avec succès
        session()->flash('success', 'Template  supprimer avec succès');
        return redirect()->route('dashboard');


        // return Inertia::render('Dashboard');
    }
}