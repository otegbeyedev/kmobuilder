<?php

namespace App\Http\Controllers\KmoController;

use App\Models\Typecomponent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Models\Kmo\Pagetemplate;
use App\Models\Kmo\Template;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Inertia\Inertia;

class AdminController extends Controller
{

    // fonction qui affiche la vue du dashboard
    public function index()
    {
        $countUsers = count(User::all()->toArray());
        $resultats = $this->statistique();
        $usersPastMonth = $resultats["countUs"];
        $namePastMonth = $resultats["nameMonth"];
        $templatesResults = Template::join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as template_id', 'templates.template_name', Pagetemplate::raw("count(templates.id) as countPages"), 'templates.status', 'users.name', 'templates.created_at')
            ->join('pagetemplates', 'pagetemplates.template_id', '=', 'templates.id')
            ->orderBy("templates.created_at", "desc")
            ->groupBy('templates.id')
            ->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->template_id),
                'template_name' => $template->template_name,
                'countPages' => $template->countPages,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);

        return Inertia::render('Admin/Index', [
            'countUsers' => $countUsers,
            'usersPastMonth' => $usersPastMonth,
            'namePastMonth' => $namePastMonth,
            'templatesResults' => $templatesResults
        ]);
    }

    // fonction qui affiche la vue de la création d'un compte administrateur
    public function superIndex()
    {
        return Inertia::render('Admin/CreateAdmin');
    }

    // fonction qui affiche la vue contenant la liste de tous les utilisateurs
    public function allsUsers()
    {
        $allUsers = User::all()->transform(fn($user) => [
            'id' => Crypt::encryptString($user->id),
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ]);
        return Inertia::render('Admin/GestionUser', ["allUsers" => $allUsers]);
    }

    // fonction permettant de modifier le rôle d'un utilisateur
    public function updateUser(Request $request, User $user)
    {
        $id = Crypt::decryptString($request->user_id);
        try {
            $user->where("id", "=", $id)->update(["role" => $request->user_role]);
            $allUsers = User::all()->transform(fn($user) => [
                'id' => Crypt::encryptString($user->id),
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role
            ]);
            return json_encode(["allUsers" => $allUsers]);
        } catch (\Exception $th) {
            dd($th);
        }
    }

    // fonction pour donner l'ordre alphabétique des noms de la table users
    public function orderUser()
    {
        $allOrders = User::orderBy('name', 'asc')->get()->transform(fn($user) => [
            'id' => Crypt::encryptString($user->id),
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ]);
        return json_encode(["allOrders" => $allOrders]);
    }

    // fonction pour donner l'ordre alphabétique des noms des pages de la table pagetemplates
    public function orderTemplateName()
    {
        $allOrders = Template::join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as template_id', 'templates.template_name', Pagetemplate::raw("count(templates.id) as countPages"), 'templates.status', 'users.name', 'templates.created_at')
            ->join('pagetemplates', 'pagetemplates.template_id', '=', 'templates.id')
            ->orderBy("templates.created_at", "asc")
            ->groupBy('templates.id')
            ->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->template_id),
                'template_name' => $template->template_name,
                'countPages' => $template->countPages,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);
        return json_encode(["allOrders" => $allOrders]);
    }

    // fonction qui inverse l'ordre alphabétique des noms des pages de la table pagetemplates
    public function orderTemplateNameDesc()
    {
        $allOrders = Template::join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as template_id', 'templates.template_name', Pagetemplate::raw("count(templates.id) as countPages"), 'templates.status', 'users.name', 'templates.created_at')
            ->join('pagetemplates', 'pagetemplates.template_id', '=', 'templates.id')
            ->orderBy("templates.created_at", "desc")
            ->groupBy('templates.id')
            ->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->template_id),
                'template_name' => $template->template_name,
                'countPages' => $template->countPages,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);
        return json_encode(["allOrders" => $allOrders]);
    }

    // fonction qui inverse l'ordre alphabétique des noms de la table users
    public function orderUserDesc()
    {
        $allOrders = User::orderBy('name', 'desc')->get()->transform(fn($user) => [
            'id' => Crypt::encryptString($user->id),
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ]);
        return json_encode(["allOrders" => $allOrders]);
    }

    // fonction qui fait une recherche dans la table users pour retourner tous les utilisateurs qui ont ces caractères dans leurs noms
    public function searchUser(Request $request)
    {
        $resultSearch = User::where('name', 'like', '%' . $request->value . '%')->get()->transform(fn($user) => [
            'id' => Crypt::encryptString($user->id),
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ]);
        return json_encode(["resultSearch" => $resultSearch]);
    }

    // fonction qui fait une recherche dans la table templates pour retourner tous les templates qui ont ces caractères dans leurs noms
    public function searchTemplateName(Request $request)
    {
        $resultSearch = Template::join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as template_id', 'templates.template_name', Pagetemplate::raw("count(templates.id) as countPages"), 'templates.status', 'users.name', 'templates.created_at')
            ->join('pagetemplates', 'pagetemplates.template_id', '=', 'templates.id')
            ->where('templates.template_name', 'like', '%' . $request->value . '%')
            ->orderBy("templates.created_at", "desc")
            ->groupBy('templates.id')
            ->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->template_id),
                'template_name' => $template->template_name,
                'countPages' => $template->countPages,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);
        return json_encode(["resultSearch" => $resultSearch]);
    }

    // fonction qui fait une recherche dans la table pagetemplates pour retourner tous les templates qui ont ces caractères dans leurs noms
    public function searchTemplateEmpty(Request $request)
    {
        // requête pour récupérer les templates qui n'ont pas de pages et qui ont ces caractères dans leurs noms
        $templatesNotPages = Template::leftJoin('pagetemplates', 'templates.id', '=', 'pagetemplates.template_id')
            ->join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as templateId', 'template_name', 'templates.status', 'templates.created_at', 'users.name')
            ->where('templates.template_name', 'like', '%' . $request->value . '%')
            ->whereNull('pagetemplates.template_id')
            ->orderBy("templates.created_at", "desc")->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->templateId),
                'template_name' => $template->template_name,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);

        $templatesNotPage = [];
        if (count($templatesNotPages) > 0) {
            for ($i = 0; $i < count($templatesNotPages); $i++) {
                $now = time();
                $dat = strtotime($templatesNotPages[$i]["created_at"]);
                $diff = abs($now - $dat);
                $second = $diff % 60;
                $diff = floor(($diff - $second) / 60);
                $minute = $diff % 60;
                $diff = floor(($diff - $minute) / 60);
                $hour = $diff % 24;
                $diff = floor(($diff - $hour) / 24);
                $day = (int) $diff;
                if ($day >= 15) {
                    // Suppression d'un template après 15 jours s'il n'a pas de pages
                    $id = Crypt::decryptString($templatesNotPages[$i]["id"]);
                    Template::destroy($id);
                }
            }

            for ($i = 0; $i < count($templatesNotPages); $i++) {
                $now = time();
                $dat = strtotime($templatesNotPages[$i]["created_at"]);
                $diff = abs($now - $dat);
                $second = $diff % 60;
                $diff = floor(($diff - $second) / 60);
                $minute = $diff % 60;
                $diff = floor(($diff - $minute) / 60);
                $hour = $diff % 24;
                $diff = floor(($diff - $hour) / 24);
                $day = (int) $diff;
                if ($day < 15) {
                    $x = 15;
                    if ($x > 0) {
                        $x = $x - $day;
                    } else {
                        $x = 0;
                    }
                    $templatesNotPage[$i]["id"] = $templatesNotPages[$i]["id"];
                    $templatesNotPage[$i]["template_name"] = $templatesNotPages[$i]["template_name"];
                    $templatesNotPage[$i]["name"] = $templatesNotPages[$i]["name"];
                    $templatesNotPage[$i]["status"] = $templatesNotPages[$i]["status"];
                    $templatesNotPage[$i]["created_at"] = $templatesNotPages[$i]["created_at"];
                    $templatesNotPage[$i]["number_day"] = $x;
                }
            }
        }
        return json_encode([
            'templatesNotPage' => $templatesNotPage
        ]);
    }

    // fonction qui gère la création d'un compte administrateur
    public function createAdmin(Request $request)
    {
        if ((trim($request->name) !== "") && (trim($request->email) !== "") && (trim($request->password) !== "") && (trim($request->confirmPass) !== "")) {
            if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                if (trim($request->password) === trim($request->confirmPass)) {
                    if (strlen($request->password) >= 8) {
                        // Vérifier si l'utilistateur existe déjà
                        $existingUser = User::where('name', $request->name)->orWhere('email', $request->email)->first();
                        if ($existingUser == null) {
                            $user = User::create([
                                'name' => $request->name,
                                'email' => $request->email,
                                'password' => Hash::make($request->password),
                                'role' => $request->role
                            ]);
                            return json_encode(["success" => "Compte créé !!!"]);
                        } else {
                            return json_encode(["error" => "Utilisateur existant !!!"]);
                        }
                    } else {
                        return json_encode(["error" => "Minimun 8 caractères pour le mot de passe !!!"]);
                    }
                } else {
                    return json_encode(["error" => "Les mots de passe ne sont pas conformes !!!"]);
                }
            } else {
                return json_encode(["error" => "L'email ne respecte pas les normes requises !!!"]);
            }
        } else {
            return json_encode(["error" => "Remplissez correctement les champs !!!"]);
        }
    }

    // Connaître le pourcentage des utilisateurs venus dans le mois passé
    public function statistique()
    {
        $allMonths = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
        $pastMonth = intval(date("m")) - 1;
        if ($pastMonth == 0) {
            $pastMonth = 12;
            $namePastMonth = $allMonths[$pastMonth - 1];
            $currentYear = intval(date("Y")) - 1;
        } else {
            $currentYear = intval(date("Y"));
            $namePastMonth = $allMonths[$pastMonth - 1];
        }
        $daysPastMonth = DaysPast::daysPastMonth($pastMonth, $currentYear);
        $datas = User::all()->toArray();
        $dat = [];
        $usersPast = 0;
        for ($i = 0; $i < count($datas); $i++) {
            $el = explode("T", $datas[$i]["created_at"])[0];
            array_push($dat, $el);
        }
        for ($i = 0; $i < count($dat); $i++) {
            if (in_array($dat[$i], $daysPastMonth)) {
                $usersPast += 1;
            }
        }
        return ["countUs" => $usersPast, "nameMonth" => $namePastMonth];
    }

    // Suppression d'un utilisateur
    public function destroy(Request $request)
    {
        // Récupérer l'ID de l'élément à supprimer à partir de la requête
        $id = Crypt::decryptString($request->userid);

        // Supprimer l'utilisateur portant cet id'
        User::destroy($id);
        $allUsers = User::all()->transform(fn($user) => [
            'id' => Crypt::encryptString($user->id),
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role
        ]);
        return json_encode(["allUsers" => $allUsers]);
    }

    // suppressin d'un template comportant des pages
    public function deleteTemplate(Request $request)
    {
        // Récupérer l'ID de l'élément à supprimer à partir de la requête
        $id = Crypt::decryptString($request->templateId);

        // Supprimer le projet lui-même
        Template::destroy($id);
        $templatesResults = Template::join('users', 'users.id', '=', 'templates.user_id')
            ->select('templates.id as template_id', 'templates.template_name', Pagetemplate::raw("count(templates.id) as countPages"), 'templates.status', 'users.name', 'templates.created_at')
            ->join('pagetemplates', 'pagetemplates.template_id', '=', 'templates.id')
            ->orderBy("templates.created_at", "desc")
            ->groupBy('templates.id')
            ->get()->transform(fn($template) => [
                'id' => Crypt::encryptString($template->template_id),
                'template_name' => $template->template_name,
                'countPages' => $template->countPages,
                'name' => $template->name,
                'status' => $template->status,
                'created_at' => $template->created_at
            ]);
        return json_encode(["templatesResults" => $templatesResults]);
    }


}