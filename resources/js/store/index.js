import { defineStore } from 'pinia';

export const dataStore = defineStore('user', {
  state: () => ({
    //declaration d'une variable dans le magasin pinia
    userTemplate: null,
    connected: null,
    builderMenuClick:null,
  }),
  
  getters: {
    //getter pour recuperer tous les templates de l'uilisateur
    getTemplateData: (state) => state.userTemplate,
    getConnectedData: (state) => state.connected,
    getBuilderMenuClick:(state)=>state.builderMenuClick,
  },

  actions: {
    //setters pour changer le contenu de la variable qui contient le nouveau template créer par l'utilisateur
    setUserData(data) {
      this.userTemplate = data;
    },
   setConnectedData(data) {
      this.connected = data;
    },

    setBuilderMenuClick ( data){
      this.builderMenuClick =data;
    },
  },
});