/** src/router/index.js **/
// import * from 'laravel-vite-plugin';
import {
    createRouter,
    createWebHistory
} from 'vue-router'
import Home from "../Pages/Home.vue";
import Builder from "../Pages/Builder.vue";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home
        },
        {
            path: "/builder",
            name: "Builder",
            component: Builder
        },
      
   

    ]

});

export default router;
